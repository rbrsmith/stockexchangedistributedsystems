package RMI;

import logger.LoggerClient;
import util.Config;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class RMIServer<T extends Remote> {

    /**
     *
     * @param obj Generic of Server to create
     * @param serviceName String to bind RMI to
     * @param port int of port to bind RMI to
     * @throws RemoteException
     */
    public void create(T obj, String serviceName, int port) throws RemoteException {
        System.setProperty("java.security.policy", Config.getInstance().loadSecurityPolicy());

        //load security policy
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new SecurityManager());
        }

        //create local rmi registry
        LocateRegistry.createRegistry(port);

        //bind service to default port portNum
        T stub =
                (T) UnicastRemoteObject.exportObject(obj, port);
        Registry registry = LocateRegistry.getRegistry(port);
        registry.rebind(serviceName, stub);
        LoggerClient.log(serviceName + " bound on " + port);
    }

}
