package stockExchange.exchange;


import RMI.RMIClient;
import business.BusinessInterface;
import common.BusinessName;
import common.Customer;
import common.Holding;
import common.ShareType;
import util.Config;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Exchange {

    Map<BusinessName, BusinessInterface> listing;

    Map<Customer, Map<BusinessInterface, Holding>> holdings;

    /**
     * Create BusinessDirectory
     * Start Business Servers
     */
    public Exchange() {
        listing = new HashMap<BusinessName, BusinessInterface>();
        holdings = new HashMap<Customer, Map<BusinessInterface, Holding>>();
        try {
            listing.put(BusinessName.GOOGLE, getBusiness(BusinessName.GOOGLE));
            listing.put(BusinessName.YAHOO, getBusiness(BusinessName.YAHOO));
            listing.put(BusinessName.MICROSOFT, getBusiness(BusinessName.MICROSOFT));
        } catch (RemoteException rme) {
            System.out.println("Remote Exception in Exchange: " + rme.getMessage());
        } catch (NotBoundException nbe) {
            System.out.println("Not Bound Exception in Exchange: " + nbe.getMessage());
        }
    }

    /**
     *
     * @param customer Customer object purchasing the shares
     * @param business BusinessInterface of business the customer wants to buy
     * @param quantity int amount of shares to buy
     * @param type ShareType of share to purchase
     */
    public void buyShares(Customer customer, BusinessInterface business, int quantity, ShareType type) {
        Map<BusinessInterface, Holding> customerHoldings = holdings.get(customer);
        if(customerHoldings == null){
            // new customer
            Holding currentHoldingOrder = new Holding(type, quantity);
            customerHoldings = new HashMap<BusinessInterface, Holding>();
            customerHoldings.put(business, currentHoldingOrder);
            holdings.put(customer, customerHoldings);
        } else {
            // Check if customer has any dealings with this business
            Holding currentHolding = customerHoldings.get(business);
            if(currentHolding == null) {
                // New business
                customerHoldings.put(business, new Holding(type, quantity));
            } else {
                currentHolding.add(type, quantity);
            }
        }
    }

    /**
     *
     * @param customer Customer object
     * @return Map of Business and the stock Holdings the customer holds
     */
    public Map<BusinessInterface, Holding> getHoldings(Customer customer) {
        return holdings.get(customer);
    }

    /**
     *
     * @return Map of all businesses indexes by BusinessName
     */
    public Map<BusinessName, BusinessInterface> getListing() {
        return listing;
    }



    /**
     *
     * @param businessName of business to locate
     * @return BusinessInterface representing the business
     * @throws RemoteException
     * @throws NotBoundException
     */
    public BusinessInterface getBusiness(BusinessName businessName) throws RemoteException, NotBoundException {

        String name = businessName.toString();
        String portName = name + "Port";
        String ipName = name + "IP";

        Integer port = Integer.parseInt(Config.getInstance().getAttr(portName));
        String ip = Config.getInstance().getAttr(ipName);

        RMIClient<BusinessInterface> rmi = new RMIClient<BusinessInterface>();
        return rmi.getService(ip, port, name);
    }

    /**
     *
     * @return ArrayList of ticker Strings listed on the exchange
     */
    public ArrayList<String> getTickers() {
        try {
            ArrayList<String> tickers = new ArrayList<String>();

            for(BusinessInterface bi : listing.values()) {
                tickers.add(bi.getTicker());
            }
            return tickers;
        } catch (RemoteException rme) {
            System.out.println("Remote Exception in Exchange.getTickers(): " + rme.getMessage());
            return null;
        }
    }

}
