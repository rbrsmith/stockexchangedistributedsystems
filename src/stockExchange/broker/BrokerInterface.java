package stockExchange.broker;

import common.Customer;
import common.Holding;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Map;

public interface BrokerInterface extends Remote {

    ArrayList<String> getTickerListing() throws RemoteException;

    boolean buyShares(Customer customer, String ticker, int quantity) throws RemoteException;

    Map<String, Holding> getCustomerHolding(Customer customer) throws RemoteException;
}
