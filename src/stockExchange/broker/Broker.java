package stockExchange.broker;

import RMI.RMIServer;
import business.BusinessInterface;
import common.BusinessName;
import common.Customer;
import stockExchange.exchange.Exchange;
import common.Holding;
import common.ShareType;
import util.Config;

import java.io.Serializable;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Broker class handles all requests from client and acts as a middle man
 */
public class Broker implements BrokerInterface, Serializable {


    public Exchange exchange;

    // Require for serialization.  Ensures object deserialize and serialized are the same.
    private static final long serialVersionUID = 1467890432560789065L;

    public Broker() {
        exchange = new Exchange();
    }

    /** Start Broker Server **/
    public static void main(String[] args) {
        try {
            RMIServer<Broker> rmiServer = new RMIServer<Broker>();
            Integer port = Integer.parseInt(Config.getInstance().getAttr("brokerPort"));
            rmiServer.create(new Broker(), "broker", port);

        } catch(RemoteException rme) {
            System.out.println("Remote Exception in Broker class: " + rme.getMessage());
        }

    }



    /**
     *
     * @return ArrayList of ticker strings
     */
    public ArrayList<String> getTickerListing() {
        return exchange.getTickers();
    }

    /**
     *
     * @param customer Customer object who wants to purchase shares
     * @param ticker String symbol of business to purchase shares
     * @param quantity int amount of shares to purchase
     * @return boolean true if success
     * @throws RemoteException
     */
    public boolean buyShares(Customer customer, String ticker, int quantity) throws RemoteException {
        // Find business from ticker
        BusinessInterface businessToBuy = getBusiness(ticker);
        if(businessToBuy != null) {
            exchange.buyShares(customer, businessToBuy, quantity, ShareType.COMMON);
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param ticker String symbol of companys ticker on the exchange
     * @return BusinessInterface of the Business with ticker
     */
    private BusinessInterface getBusiness(String ticker) {
        Map<BusinessName, BusinessInterface> listing = exchange.getListing();
        for(BusinessName businessName : listing.keySet()) {
            if(businessName.getTicker().equals(ticker)){
                return listing.get(businessName);

            }
        }
        return null;
    }

    /**
     *
     * @param customer Customer object to retrieve holdings for
     * @return Map of Customer holdings for each String ticker
     * @throws RemoteException
     */
    public Map<String, Holding> getCustomerHolding(Customer customer) throws RemoteException{
        Map<BusinessInterface, Holding> customerHoldings = exchange.getHoldings(customer);

        Map<String, Holding> simplifiedHoldings = new HashMap<String, Holding>();
        if(customerHoldings != null) {
            for (Map.Entry<BusinessInterface, Holding> entry : customerHoldings.entrySet()) {
                simplifiedHoldings.put(entry.getKey().getTicker(), entry.getValue());
            }
        }
        return simplifiedHoldings;
    }

}
