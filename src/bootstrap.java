import business.Business;
import logger.LoggerServer;
import stockExchange.broker.Broker;

/**
 * Created by b0467851 on 6/8/2015.
 */
public class bootstrap {

    public static void main(String args[]) throws Exception{
        (new Thread() {
            public void run(){
                LoggerServer.main(null);
            }
        }).start();
        Thread.sleep(1000);
        (new Thread() {
            public void run(){
                Business.main(null);
            }
        }).start();

        Thread.sleep(1000);
        (new Thread() {
            public void run(){
                Broker.main(null);
            }
        }).start();
    }


}
