package util;

import org.json.JSONObject;

import java.io.*;

/**
 * Class reads in from config.json in util package
 * Is used to get any configuration items set in this json file
 */
public class Config {

    private static class Holder{
        static final Config INSTANCE = new Config();
    }

    /**
     *
     * @return Config instance
     */
    public static Config getInstance() {
        return Holder.INSTANCE;
    }


    private JSONObject configJson;

    public Config() {
        try {
            // Read config file
            setJson();
        } catch(Exception ioe){
            System.out.println("IOE ERROR: " + ioe.getMessage());
        }
    }


    /**
     * Gets the json file creates a json object for reference
     * @throws IOException
     */
    private void setJson() throws Exception {
        configJson = null;

        InputStream in = Thread.currentThread().getContextClassLoader().getResourceAsStream("config.json");

        // File f = new File("C:\\Users\\Ross\\Dropbox\\Distributed_Systems\\project\\src\\config.json");
        //  InputStream in = new FileInputStream(f);

        BufferedReader reader = new BufferedReader(new InputStreamReader(in));
        StringBuilder out = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            out.append(line);
        }

        String configString = out.toString();   //Prints the string content read from input stream
        reader.close();
        configJson = new JSONObject(configString);
    }


    /**
     *
     * @param attr String name of item in config.json we are trying to retrieve
     * @return String of json object or null if attribute doesn't exist.
     */
    public String getAttr(String attr) {
        try {
            String attribute = configJson.getString(attr);
            return attribute.replace("/", File.separator);
        } catch(org.json.JSONException joe) {
            System.out.println("Json Exception in config when trying to get " + attr);
            return null;
        }
    }

    /**
     * Get security policy for RMIServer communication
     * @return String of location to security policy
     */
    public String loadSecurityPolicy() {
            return this.configJson.getString("projectHome") + configJson.getString("security");
    }

}
