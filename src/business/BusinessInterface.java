package business;

import java.rmi.Remote;
import java.rmi.RemoteException;


public interface BusinessInterface extends Remote {

    public String getName() throws RemoteException;

    public String getTicker() throws RemoteException;

}
