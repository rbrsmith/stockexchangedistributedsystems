package business;

import RMI.RMIServer;
import common.BusinessName;
import util.Config;

import java.io.Serializable;
import java.rmi.RemoteException;


public class Business implements BusinessInterface, Serializable {

    String name;
    String ticker;
    private static RMIServer<Business> rmiServer = new RMIServer<Business>();

    /**
     *
     * @param name BusinessName enum of business to create
     */
    public Business(BusinessName name) {
        this.name = name.toString();
        this.ticker = name.getTicker();
    }

    /**
     *
     * @return String business name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @return String business exchange symbol
     */
    public String getTicker() {
        return ticker;

    }

    /**
     * Start up all business servers
     * Each business has their own service running
     * @param args
     */
    public static void main(String[] args) {
        try {
            createBusiness(BusinessName.GOOGLE);
            createBusiness(BusinessName.YAHOO);
            createBusiness(BusinessName.MICROSOFT);
        } catch (RemoteException rme) {
            System.out.println("Remote Exception in business: " + rme.getMessage());
        }
    }

    /**
     *
     * @param businessName BusinessName enum to construct business for
     * @return Business object that has a server running
     * @throws RemoteException
     */
    public static Business createBusiness(BusinessName businessName) throws RemoteException {

        Business business = new Business(businessName);

        String portName = business.getName() + "Port";
        Integer port = Integer.parseInt(Config.getInstance().getAttr(portName));

        rmiServer.create(business, business.getName(), port);
        return business;

    }



}
