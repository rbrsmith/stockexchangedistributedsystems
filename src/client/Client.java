package client;

import RMI.RMIClient;
import stockExchange.broker.BrokerInterface;
import util.Config;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;

/**
 * Client application that runs locally
 */
public class Client {

    /**
     *
     * @return BrokerInterface of broker service
     * @throws RemoteException
     * @throws NotBoundException
     */
    public BrokerInterface getBroker() throws RemoteException, NotBoundException {

        RMIClient<BrokerInterface> rmiClient = new RMIClient<BrokerInterface>();

        String host = Config.getInstance().getAttr("brokerServerIP");
        Integer port = Integer.parseInt(Config.getInstance().getAttr("brokerPort"));

        return rmiClient.getService(host, port, "broker");
    }


}
