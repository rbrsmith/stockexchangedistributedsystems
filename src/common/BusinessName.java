package common;

/**
 * Created by b0467851 on 6/8/2015.
 */
public enum BusinessName {

    MICROSOFT("microsoft", "MSFT"),
    GOOGLE("google", "GOOG"),
    YAHOO("yahoo", "YHOOS");

    private final String name;
    private final String ticker;

    private BusinessName(String s, String t) {
        name = s;
        ticker = t;
    }





    public String toString() {
        return name;
    }

    public String getTicker(){
        return ticker;
    }
}
