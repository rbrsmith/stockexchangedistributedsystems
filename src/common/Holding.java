package common;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Wrapper of a map, representing the amount of each ShareType is held
 */
public class Holding implements Serializable{

    Map<ShareType, Integer> holding;
    private static final long serialVersionUID = 1224582049983663095L;

    public Holding(ShareType type, int quantity) {
        holding = new HashMap<ShareType, Integer>();
        holding.put(type, quantity);
    }

    /**
     *
     * @param type ShareType to add or set quantity to
     * @param quantity int amount of that ShareType purchased
     * @return
     */
    public int add(ShareType type, int quantity) {
        Integer current = holding.get(type);
        if(current == null) {
            current = 0;
        }

        int total = current + quantity;
        holding.put(type, total);
        return total;
    }


    @Override
    public String toString() {
        return holding.toString();
    }

    /**
     *
     * @param type ShareType
     * @return int amount of that ShareType | 0 if none is held
     */
    public int getQuantity(ShareType type) {
        Integer quantity = holding.get(type);
        if(quantity != null) {
            return quantity;
        } else {
            return 0;
        }
    }

}
