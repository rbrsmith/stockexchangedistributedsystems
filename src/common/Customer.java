package common;

import java.io.Serializable;

/**
 * Customer holding all simple information about a customer who desires to purchase shares
 */
public class Customer implements Serializable {

    private static final long serialVersionUID = 1425572349963361090L;
    private String name;

    /**
     *
     * @param name String of customer's name
     */
    public Customer(String name){
        this.name = name;
    }

    /**
     *
     * @return String of customers name
     */
    public String getName(){
        return name;
    }

    @Override
    public String toString(){
        return getName();
    }

   @Override
   public boolean equals(Object obj) {
        if(!(obj instanceof Customer)) {
            return false;
        }
        if(obj == this) {
            return true;
        }

        Customer customer = (Customer) obj;

       return customer.getName().equals(getName());
    }

    @Override
    public int hashCode() {
        return 3 * 50 + name.hashCode();
    }

}
