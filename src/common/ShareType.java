package common;

/**
 * ENUM of all the types of shares that exist
 */
public enum ShareType {

    COMMON,
    PREFERRED,
    CONVERTIBLE;
}
