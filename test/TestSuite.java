import client.ClientTest;
import logger.LoggerClientTest;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import stockExchange.broker.BrokerTest;
import stockExchange.exchange.Exchange;

@RunWith(Suite.class)
@Suite.SuiteClasses({
        ClientTest.class,
        LoggerClientTest.class,
        BrokerTest.class
})
public class TestSuite {

    static Thread thread;

    @BeforeClass
    public static void setUpClass()  {
        thread = new Thread() {
                public void run() {
                    try {
                        bootstrap.main(null);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            };
        thread.start();
        try {
            Thread.sleep(1000);
        } catch(Exception e){
            e.printStackTrace();
        }
    }
    @AfterClass
    public static void tearDownClass() {
        thread.interrupt();

    }
}