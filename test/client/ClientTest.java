package client;

import common.Customer;
import org.junit.Before;
import org.junit.Test;
import common.Holding;
import common.ShareType;

import java.util.Map;

import static org.junit.Assert.*;


public class ClientTest {

    Client client;

    @Before
    public void setUp() throws Exception {
        client = new Client();
    }

    @Test
    public void testGetBroker()  {
        try {
            assertNotNull(client.getBroker());
        } catch(Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void getTickers() throws Exception {
        assertEquals(client.getBroker().getTickerListing().size(), 3);
    }

    @Test
    public void buySharesTest() throws Exception {
        Customer customer = new Customer("Ross");

        String ticker = "GOOG";
        int quantity  = 100;

        Map<String, Holding> holding = client.getBroker().getCustomerHolding(customer);
        assertTrue(holding.isEmpty());

        assertTrue(client.getBroker().buyShares(customer, ticker, quantity));

        holding = client.getBroker().getCustomerHolding(customer);
        assertEquals(holding.get(ticker).getQuantity(ShareType.COMMON), quantity);

        customer = new Customer("Bob");

        ticker = "MSFT";
        quantity = 150;


        assertTrue(client.getBroker().buyShares(customer, ticker, quantity));

        holding = client.getBroker().getCustomerHolding(customer);
        assertEquals(holding.get(ticker).getQuantity(ShareType.COMMON), quantity);


        assertTrue(client.getBroker().buyShares(customer, ticker, quantity));

        holding = client.getBroker().getCustomerHolding(customer);
        assertEquals(holding.get(ticker).getQuantity(ShareType.COMMON), quantity * 2);


    }
}