package logger;

import org.junit.Test;
import util.Config;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

import static org.junit.Assert.*;

/**
 * Test logger
 */
public class LoggerClientTest {

    @Test
    public void testLog() throws Exception {
        String uuid = UUID.randomUUID().toString();
        LoggerClient.log(uuid);
        // Give server threads chance to complete
        Thread.sleep(1000);
        String lastLine = getLastLine();
        assertTrue(lastLine.contains(uuid));

        TimerLoggerClient tlc = new TimerLoggerClient();
        tlc.start();
        tlc.end();

        Thread.sleep(1000);
        lastLine = getLastLine();
        assertTrue(lastLine.contains("Time stopped"));
    }

    /**
     *
     * @return String last line of log file
     * @throws Exception
     */
    private String getLastLine() throws Exception {
        String logFile = new String(Files.readAllBytes(Paths.get(
                Config.getInstance().getAttr("projectHome") + Config.getInstance().getAttr("logServerFile"))));

        String[] splitFile = logFile.split("\n");
        return splitFile[splitFile.length - 1];
    }
}